﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesApp.API.DTO;
using MoviesApp.API.Services;
using MoviesApp.DAL;
using MoviesApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private MovieService mService;

        public MovieController(MovieService mService)
        {
            this.mService = mService;
        }

        [HttpGet]
        public IActionResult GetMovies([FromQuery] MovieFilterDTO filter)
        {
            return Ok(mService.Read(filter));
        }

        [HttpPost]
        public IActionResult AddMovie(MovieAddDTO form)
        {
            mService.Create(form);
            return NoContent();
        }

        [HttpGet("{id}")]
        public IActionResult GetMovie(int id)
        {
            MovieDetailsDTO dto = mService.GetByID(id);
            if (dto == null) return NotFound();
            return Ok(dto);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateMovie(int id)
        {
            // se connecter à la db pour modifier un film
            return Ok(id);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteMovie(int id)
        {
            // se connecter à la db pour supprimer un film
            return Ok(id);
        }
    }
}
