﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesApp.API.DTO
{
    public class MovieDetailsDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public string Poster { get; set; }
        public int Duration { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
