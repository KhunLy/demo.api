﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesApp.API.DTO
{
    public class MovieAddDTO
    {
        [Required]
        [MaxLength(255)]
        public string Title {  get; set; }
        
        [Required]
        [Range(60, 86400)]
        public int Duration { get; set; }
        public int? CategoryId { get; set; }

        [Url]
        public string Poster { get; set; }
    }
}
