﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesApp.API.DTO
{
    public class MovieFilterDTO
    {
        [Range(5,100)]
        public int Limit { get; set; }
        public int Offset { get; set; }
        public int? CategoryId { get; set; }
    }
}
