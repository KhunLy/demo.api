﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesApp.API.DTO
{
    public class MovieIndexDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Poster { get; set; }
    }
}
