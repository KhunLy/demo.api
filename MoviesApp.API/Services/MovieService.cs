﻿using Microsoft.EntityFrameworkCore;
using MoviesApp.API.DTO;
using MoviesApp.DAL;
using MoviesApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolBox.AutoMapper.Mappers;

namespace MoviesApp.API.Services
{
    public class MovieService
    {
        private readonly MovieDBContext dc;

        public MovieService(MovieDBContext dc)
        {
            this.dc = dc;
        }

        public void Create(MovieAddDTO dto)
        {
            //dc.Movies.Add(new Movie
            //{
            //    Title = dto.Title,
            //    Duration = dto.Duration,
            //    Poster = dto.Poster,
            //    CategoryId = dto.CategoryId
            //});
            Movie m = dc.Movies.Add(dto.MapTo<Movie>()).Entity;
            dc.SaveChanges();
            int id = m.Id;
        }

        public IEnumerable<MovieIndexDTO> Read(MovieFilterDTO filter)
        {
            //return dc.Movies
            //    .Skip(filter.Offset)
            //    .Take(filter.Limit)
            //    .Select(m => new MovieIndexDTO { 
            //        Id = m.Id,
            //        Poster = m.Poster,
            //        Title = m.Title
            //});
            return dc.Movies
                .Where(m => filter.CategoryId == null || m.CategoryId == filter.CategoryId)
                .Skip(filter.Offset)
                .Take(filter.Limit)
                .MapToList<MovieIndexDTO>();
        }

        public MovieDetailsDTO GetByID(int id)
        {
            Movie movie = dc.Movies
                .Include(m => m.Category)
                .FirstOrDefault(m => m.Id == id);
            return movie?.MapTo<MovieDetailsDTO>(m => m.CategoryName = movie.Category?.Name);
        }
    }
}
