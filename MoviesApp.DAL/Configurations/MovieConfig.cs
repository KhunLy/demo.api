﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoviesApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesApp.DAL.Configurations
{
    class MovieConfig : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            // nommer la table
            builder.ToTable("Movies");

            // definir la primary key
            builder.HasKey(m => m.Id);

            // specifier que la colonne doit etre autoIncrementée
            builder.Property(m => m.Id).ValueGeneratedOnAdd();

            // modifier les contraintes des colonnes de la table
            builder.Property(m => m.Title)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);


            // ajout d'une contrainte de validation
            //builder.HasCheckConstraint("CK_duration", "Duration < 1000");

            builder.HasOne(m => m.Category)
                .WithMany(c => c.Movies)
                .OnDelete(DeleteBehavior.SetNull)
                .HasForeignKey(m => m.CategoryId);

        }
    }
}
