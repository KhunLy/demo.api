﻿using Microsoft.EntityFrameworkCore;
using MoviesApp.DAL.Configurations;
using MoviesApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesApp.DAL
{
    public class MovieDBContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=K-PC;initial catalog=MovieDB;integrated security=true");
            //optionsBuilder.UseSqlServer("server=;initial catalog=;uid=sa;pwd=test1234");
        }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.ApplyConfiguration(new CategoryConfig());
            mb.ApplyConfiguration(new MovieConfig());
        }
    }
}
